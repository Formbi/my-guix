(define-module (formbi packages pypy)
  #:use-module (gnu packages python)
  #:use-module (guix packages)
  #:use-module (guix download)
  )

(define-public pypy2
  (package
   (inherit pypy3)
   (name "pypy2")
   (version "7.3.1")
   (source (origin
              (method url-fetch)
              (uri (string-append "https://downloads.python.org/pypy/"
                                  "pypy2.7-v" version "-src.tar.bz2"))
              (sha256
               (base32
                "08ckkhd0ix6j9873a7gr507c72d4cmnv5lwvprlljdca9i8p2dzs"))))
   (arguments
     `(#:tests? #f     ;FIXME: Disabled for now, there are many tests failing.
       #:modules ((ice-9 ftw) (ice-9 match)
                  (guix build utils) (guix build gnu-build-system))
       #:phases (modify-phases %standard-phases
                  (delete 'configure)
                  (add-after 'unpack 'patch-source
                    (lambda* (#:key inputs outputs #:allow-other-keys)
                      (substitute* '("rpython/rlib/clibffi.py")
                        ;; find_library does not work for libc
                        (("ctypes\\.util\\.find_library\\('c'\\)") "'libc.so'"))
                      (substitute* '("lib_pypy/cffi/_pycparser/ply/cpp.py")
                        ;; Make reproducible (XXX: unused?)
                        (("time\\.localtime\\(\\)") "time.gmtime(0)"))
                      (substitute* '("pypy/module/sys/version.py")
                        ;; Make reproducible
                        (("t\\.gmtime\\(\\)") "t.gmtime(0)"))
                      (substitute* '("lib_pypy/_tkinter/tklib_build.py")
                        ;; Link to versioned libtcl and libtk
                        (("linklibs = \\['tcl', 'tk'\\]")
                         "linklibs = ['tcl8.6', 'tk8.6']")
                        (("incdirs = \\[\\]")
                         (string-append "incdirs = ['"
                                        (assoc-ref inputs "tcl")
                                        "/include', '"
                                        (assoc-ref inputs "tk")
                                        "/include']")))
                      (substitute* '("lib_pypy/_curses_build.py")
                        ;; Find curses
                        (("/usr/local") (assoc-ref inputs "ncurses")))
                      (substitute* '("lib_pypy/_sqlite3_build.py")
                        ;; Always use search paths
                        (("sys\\.platform\\.startswith\\('freebsd'\\)") "True")
                        ;; Find sqlite3
                        (("/usr/local") (assoc-ref inputs "sqlite"))
                        (("libname = 'sqlite3'")
                         (string-append "libname = '"
                                        (assoc-ref inputs "sqlite")
                                        "/lib/libsqlite3.so.0'")))
                      (substitute* '("lib-python/2.7/subprocess.py")
                        ;; Fix shell path
                        (("/bin/sh")
                         (string-append (assoc-ref inputs "bash-minimal") "/bin/sh")))
                      (substitute* '("lib-python/2.7/distutils/unixccompiler.py")
                        ;; gcc-toolchain does not provide symlink cc -> gcc
                        (("\"cc\"") "\"gcc\""))
                      #t))
                  (add-after
                      'unpack 'set-source-file-times-to-1980
                    ;; copied from python package, required by zip testcase
                    (lambda _
                      (let ((circa-1980 (* 10 366 24 60 60)))
                        (ftw "." (lambda (file stat flag)
                                   (utime file circa-1980 circa-1980)
                                   #t))
                        #t)))
                  (replace 'build
                    (lambda* (#:key inputs #:allow-other-keys)
                      (with-directory-excursion "pypy/goal"
                        ;; Build with jit optimization.
                        (invoke "python2"
                                "../../rpython/bin/rpython"
                                (string-append "--make-jobs="
                                               (number->string (parallel-job-count)))
                                "-Ojit"
                                "targetpypystandalone"))
                      ;; Build c modules and package everything, so tests work.
                      (with-directory-excursion "pypy/tool/release"
                        (unsetenv "PYTHONPATH") ; Do not use the system’s python libs:
                                        ; AttributeError: module 'enum' has no
                                        ; attribute 'IntFlag'
                        (invoke "python2" "package.py"
                                "--archive-name" "pypy-dist"
                                "--builddir" (getcwd)))))
                  (replace 'check
                    (lambda* (#:key tests? #:allow-other-keys)
                      (if tests?
                          (begin
                            (setenv "HOME" "/tmp") ; test_with_pip tries to
                                        ; access ~/.cache/pip
                            ;; Run library tests only (no interpreter unit
                            ;; tests). This is what Gentoo does.
                            (invoke
                             "python2"
                             "pypy/test_all.py"
                             "--pypy=pypy/tool/release/pypy-dist/bin/pypy2"
                             "lib-python"))
                          (format #t "test suite not run~%"))
                      #t))
                  (replace 'install
                    (lambda* (#:key inputs outputs #:allow-other-keys)
                      (with-directory-excursion "pypy/tool/release"
                        ;; Delete test data.
                        (for-each
                         (lambda (x)
                           (delete-file-recursively (string-append
                                                     "pypy-dist/lib-python/2.7/" x)))
                         '("tkinter/test"
                           "test"
                           "sqlite3/test"
                           "lib2to3/tests"
                           "idlelib/idle_test"
                           "distutils/tests"
                           "ctypes/test"
                           "unittest/test"))
                        ;; Patch shebang referencing python2
                        (substitute* '("pypy-dist/lib-python/2.7/cgi.py"
                                       "pypy-dist/lib-python/2.7/encodings/rot_13.py")
                          (("#!.+/bin/python")
                           (string-append "#!" (assoc-ref outputs "out") "/bin/pypy2")))
                        (with-fluids ((%default-port-encoding "ISO-8859-1"))
                          (substitute* '("pypy-dist/lib_pypy/_md5.py"
                                         "pypy-dist/lib_pypy/_sha1.py")
                            (("#!.+/bin/python")
                             (string-append "#!" (assoc-ref outputs "out") "/bin/pypy2"))))
                        (copy-recursively "pypy-dist" (assoc-ref outputs "out")))
                      #t)))))
   (synopsis "Python 2 implementation with just-in-time compilation")))
pypy2
