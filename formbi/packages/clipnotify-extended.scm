(define-module (formbi packages clipnotify-extended)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (guix packages)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system glib-or-gtk)
  #:use-module (guix build-system meson)
  #:use-module (guix build-system python)
  #:use-module (guix build-system scons)
  #:use-module (gnu packages)
  #:use-module (gnu packages documentation)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages algebra)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages check)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages image)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages flex)
  #:use-module (gnu packages freedesktop)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages haskell-xyz)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages man)
  #:use-module (gnu packages maths)
  #:use-module (gnu packages m4)
  #:use-module (gnu packages ncurses)
  #:use-module (gnu packages perl)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages guile)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages xorg)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages bison)
  #:use-module (gnu packages xdisorg)
  #:use-module (ice-9 match))

(define-public xsel
  (package
    (name "xsel")
    (version "1.2.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://www.vergenet.net/~conrad/software"
                                  "/xsel/download/xsel-" version ".tar.gz"))
              (sha256
               (base32
                "070lbcpw77j143jrbkh0y1v10ppn1jwmjf92800w7x42vh4cw9xr"))))
    (build-system gnu-build-system)
    (inputs
     `(("libxt" ,libxt)))
    (home-page "http://www.vergenet.net/~conrad/software/xsel/")
    (synopsis "Manipulate X selection")
    (description
     "XSel is a command-line program for getting and setting the contents of
the X selection.  Normally this is only accessible by manually highlighting
information and pasting it with the middle mouse button.

XSel reads from standard input and writes to standard output by default,
but can also follow a growing file, display contents, delete entries and more.")
    (license (license:x11-style "file://COPYING"
                                "See COPYING in the distribution."))))

(define-public clipnotify-extended
  (let ((commit "2aec101d4e87ca0d0d540b4d22ec991c8dbf001a")
        (revision "1"))
    (package
      (name "clipnotify-extended")
      (version (string-append "1.0.2-"
                              revision "." (string-take commit 7)))
      (source
       (origin
         (method git-fetch)
         (uri (git-reference
               (url "https://github.com/andreblanke/clipnotify.git")
               (commit commit)))
         (file-name (git-file-name name version))
         (sha256
          (base32
           "05s7182rgyli2rqyj76v3j256x6h87jx3pfh9mnfkfpg88mbikzs"))))
      (build-system gnu-build-system)
      (arguments
       `(#:phases
         (modify-phases %standard-phases
           (delete 'configure)
           (replace 'install
             (lambda* (#:key inputs outputs #:allow-other-keys)
               (let* ((out  (assoc-ref outputs "out"))
                      (bin  (string-append out "/bin"))
                      (doc  (string-append %output "/share/doc/" ,name "-" ,version)))
                 (install-file "clipnotify" bin)
                 (install-file "README.md" doc)
                 #t))))
         #:make-flags (list "CC=gcc")
         #:tests? #f))
      (inputs
       `(("libx11" ,libx11)
         ("libXfixes" ,libxfixes)))
      (home-page "https://github.com/andreblanke/clipnotify")
      (synopsis "Notify on new X clipboard events")
      (description "@command{clipnotify} is a simple program that, using the
XFIXES extension to X11, waits until a new selection is available and then
exits.

It was primarily designed for clipmenu, to avoid polling for new selections.

This version is extended to let the user choose which selections to monitor.
The selections to be monitored by clipnotify may now be specified via 
clipnotify -s clipboard,primary,secondary.
If the -s option is not present all selections are monitored.


@command{clipnotify} doesn't try to print anything about the contents of the
selection, it just exits when it changes.  This is intentional -- X11's
selection API is verging on the insane, and there are plenty of others who
have already lost their sanity to bring us xclip/xsel/etc.  Use one of those
tools to complement clipnotify.")
      (license license:public-domain))))

(define-public clipmenu-clipboard
  (let ((commit "a495bcc7a4ab125182a661c5808364f66938a87c")
        (revision "1"))
    (package
     (name "clipmenu-clipboard")
     (version (string-append "5.6.0-"
                             revision "." (string-take commit 7)))
     (source
      (origin
        (method git-fetch)
        (uri (git-reference
              (url "https://github.com/cdown/clipnotify.git")
              (commit commit)))
        (file-name (git-file-name name version))
        (sha256
         (base32
          "12vvircdhl4psqi51cnfd6bqy85v2vwfcmdq1mimjgng727nwzys"))))
     (build-system gnu-build-system)
     (arguments
     `(#:phases
       (modify-phases %standard-phases
         (add-after 'unpack 'fix-hardcoded-paths
           (lambda _
             (substitute* "clipmenud"
               (("has_clipnotify=0")
                "has_clipnotify=1")
               (("command -v clipnotify >/dev/null 2>&1 && has_clipnotify=1")
                "")
               (("clipnotify -s clipboard \\|\\| .*")
                (string-append (which "clipnotify") "\n"))
               (("xsel --logfile")
                (string-append (which "xsel") " --logfile")))
             (substitute* "clipmenu"
               (("xsel --logfile")
                (string-append (which "xsel") " --logfile")))
             #t))
         (delete 'configure)
         (delete 'build)
         (replace 'install
           (lambda* (#:key inputs outputs #:allow-other-keys)
             (let* ((out  (assoc-ref outputs "out"))
                    (bin  (string-append out "/bin"))
                    (doc  (string-append %output "/share/doc/"
                                         ,name "-" ,version)))
               (install-file "clipdel" bin)
               (install-file "clipmenu" bin)
               (install-file "clipmenud" bin)
               (install-file "README.md" doc)
               #t))))
       #:tests? #f))
     (inputs
      `(("clipnotify" ,clipnotify-extended)
        ("xsel" ,xsel)))
     (home-page "https://github.com/cdown/clipmenu")
     (synopsis "Simple clipboard manager using dmenu or rofi and xsel (this variant uses the clipboard only)")
     (description "Start @command{clipmenud}, then run @command{clipmenu} to
select something to put on the clipboard.

This variant uses the clipboard and not the selection.

When @command{clipmenud} detects changes to the clipboard contents, it writes
them out to the cache directory.  @command{clipmenu} reads the cache directory
to find all available clips and launches @command{dmenu} (or @command{rofi},
depending on the value of @code{CM_LAUNCHER}) to let the user select a clip.
After selection, the clip is put onto the PRIMARY and CLIPBOARD X selections.")
     (license license:public-domain))))

